﻿using DevClothing.Entities;
using DevClothing.Services;
using EcommerceWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcommerceWebsite.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string search, int? PageNo)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();

            //model.PageNo = PageNo.HasValue ? PageNo.Value > 0 ? PageNo.Value :1: 1;
            model.Products = ProductsService.publicInstance.GetProducts();
            if (!string.IsNullOrEmpty(search))
            {
                model.SearchTerm = search;
                model.Products = model.Products.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            NewProductViewModels model = new NewProductViewModels();
            model.AvailableCategories = CategoriesService.publicInstance.GetCategories();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Create(NewProductViewModels model)
        {            
            var newProduct = new Product();
            newProduct.Description  = model.Description;
            newProduct.Price        = model.Price;
            newProduct.Name         = model.Name;
            newProduct.Category     = CategoriesService.publicInstance.GetCategory(model.CategoryID);

            ProductsService.publicInstance.SaveProduct(newProduct);
            return RedirectToAction("ProductTable");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditProductViewModel model = new EditProductViewModel();
            var product = ProductsService.publicInstance.GetProduct(ID);
            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.Price = product.Price;
            model.CategoryID = product.Category != null ? product.Category.ID : 0;
            model.AvailableCategories = CategoriesService.publicInstance.GetCategories();
            return PartialView(product);
        }
        [HttpPost]
        public ActionResult Edit(EditProductViewModel model)
        {
            var existingProduct = ProductsService.publicInstance.GetProduct(model.ID);
            existingProduct.Name = model.Name;
            existingProduct.Description = model.Description;
            existingProduct.Price = model.Price;
            existingProduct.Category = CategoriesService.publicInstance.GetCategory(model.ID);
            ProductsService.publicInstance.UpdateProduct(existingProduct);
            return RedirectToAction("ProductTable");
        }
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            ProductsService.publicInstance.DeleteProduct(ID);
            return RedirectToAction("ProductTable");
        }
    }
}