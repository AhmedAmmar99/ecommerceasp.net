﻿using DevClothing.Entities;
using DevClothing.Services;
using EcommerceWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcommerceWebsite.Controllers
{
    public class CategoryController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CategoryTable(string search)
        {
            CategorySearchViewModel model = new CategorySearchViewModel();

            model.Categories = CategoriesService.publicInstance.GetCategories();

            if (!string.IsNullOrEmpty(search))
            {
                model.SearchTerm = search;

                model.Categories = model.Categories.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            }

            return PartialView("CategoryTable", model);
        }

        // GET: Category
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(NewCategoryViewModel model)
        {
            var newCategory = new Category();
            newCategory.Name = model.Name;
            newCategory.Description = model.Description;
            newCategory.ImageURL = model.ImageURL;
            newCategory.isFeatured = model.isFeatured;
            CategoriesService.publicInstance.SaveCategory(newCategory);
            return RedirectToAction("CategoryTable");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditCategoryViewModel model = new EditCategoryViewModel();
            var category = CategoriesService.publicInstance.GetCategory(ID);
            model.ID            = category.ID;
            model.Name          = category.Name;
            model.Description   = category.Description;
            model.ImageURL      = category.ImageURL;
            model.isFeatured    = category.isFeatured;

            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Edit(EditCategoryViewModel model)
        {
            var existingCategory = CategoriesService.publicInstance.GetCategory(model.ID);
            existingCategory.Name           = model.Name;
            existingCategory.Description    = model.Description;
            existingCategory.ImageURL       = model.ImageURL;
            existingCategory.isFeatured     = model.isFeatured;
            CategoriesService.publicInstance.UpdateCategory(existingCategory);
            return RedirectToAction("CategoryTable");
        }
       
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            CategoriesService.publicInstance.DeleteCategory(ID);
            return RedirectToAction("CategoryTable");
        }
    }
}