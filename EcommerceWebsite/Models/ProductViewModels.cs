﻿using DevClothing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommerceWebsite.Models
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryID { get; set; }

        public List<Category> AvailableCategories { get; set; }
    }
    public class NewProductViewModels:ProductViewModel
    {   }
    public class EditProductViewModel:ProductViewModel
    {
        public int ID { get; set; }   
    }
    public class ProductSearchViewModel
    {
        public int PageNo { get; set; }
        public List<Product> Products { get; set; }
        public string SearchTerm { get; set; }
    } 
    
}