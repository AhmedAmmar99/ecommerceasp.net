﻿using DevClothing.Database;
using DevClothing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevClothing.Services
{
    class ConfigurationService
    {
        #region Singleton Pattern
        /*SingleTon Method is used.From which instance is created once 
         and will be allocated memory, when the first instance is 
         form*/

        public static ConfigurationService publicInstance
        {
            get
            {
                if (privateInstance == null) privateInstance = new ConfigurationService();
                return privateInstance;
            }
        }
        private static ConfigurationService privateInstance { get; set; }
        private ConfigurationService()
        { }
        #endregion
        public Config GetConfig(string key)
        {
            using (var context = new DevClothingContext())
            {
                return context.Configurations.Find(key);
            }
        }
    }
}
