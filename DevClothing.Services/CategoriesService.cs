﻿using DevClothing.Database;
using DevClothing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DevClothing.Services
{
    public class CategoriesService
    {
        #region Singleton Pattern
        /*SingleTon Method is used.From which instance is created once 
         and will be allocated memory, when the first instance is 
         form*/

        public static CategoriesService publicInstance
        {
            get
            {
                if (privateInstance == null) privateInstance = new CategoriesService();
                return privateInstance;
            }
        }
        private static CategoriesService privateInstance { get; set; }
        private CategoriesService()
        { }
        #endregion
        public Category GetCategory(int ID)
        {
            using (var context = new DevClothingContext())
            {
                return context.Categories.Find(ID);
            }
        }
        public List<Category> GetCategories()
        {
            using (var context = new DevClothingContext())
            {
                return context.Categories.Include(x => x.Products).ToList();
            }
        }
        public List<Category> GetFeaturedCategories()
        {
            using (var context = new DevClothingContext())
            {
                return context.Categories.Where(x => x.isFeatured && x.ImageURL != null).ToList();
            }
        }
        public void SaveCategory(Category category)
        {
            using (var context = new DevClothingContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new DevClothingContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCategory(int ID)
        {
            using (var context = new DevClothingContext())
            {
                //context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                var category = context.Categories.Find(ID);
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
