﻿using DevClothing.Database;
using DevClothing.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevClothing.Services
{
    public class ProductsService
    {
        #region Singleton Pattern
        /*SingleTon Method is used.From which instance is created once 
         and will be allocated memory, when the first instance is 
         form*/

        public static ProductsService publicInstance {
            get {
                if (privateInstance == null) privateInstance = new ProductsService();
                return privateInstance;
            }
        }
        private static ProductsService privateInstance { get; set; }
        private ProductsService()
        { }
        #endregion
        public Product GetProduct(int ID)
        {
            using (var context = new DevClothingContext())
            {
                return context.Products.Where(x=>x.ID == ID).Include(x=> x.Category).FirstOrDefault();
            }
            
        }
        public List<Product> GetProducts()
        {
            var PageSize = 10;
            using (var context = new DevClothingContext())
            {
                //return context.Products.OrderBy(c => c.ID).Skip((PageNo -1)*PageSize).Include(c => c.Category).ToList();
                return context.Products.Include(c => c.Category).ToList();
            }
        }
        public void SaveProduct(Product product)
        {
            using (var context = new DevClothingContext())
            {
                context.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;
                context.Products.Add(product);
                context.SaveChanges();
            }
        }
        public void UpdateProduct(Product product)
        {
            using (var context = new DevClothingContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteProduct(int ID)
        {
            using (var context = new DevClothingContext())
            {
                //context.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                var product = context.Products.Find(ID);
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }
    }
}
